from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from graph.models import Edge
from decimal import *
from graph.node import Node
from django import forms
import simplejson as json
import random


def index(request):   
    #central = random.choice(list(Edge.objects.filter(name__exact='')))    
    central = list(Edge.objects.filter(name__exact=''))[0]
    edges, nodes = neighborhood(central,4)
    context = {'edges': edges_tojson(edges, central.id),
        'nodes' : nodes_tojson(nodes),
        'central': central.id}
    return render(request,'graph/index.html',context)


@csrf_exempt
def special(request):    
    edge_id = request.POST.get("edge","1")
    level = int(request.POST.get("level", 2))
    if level > 4 or level < 2:
        level = 3
    central = Edge.objects.get(id=edge_id)
    edges, nodes = neighborhood(central,level)
    answer = '{"edges" : %s, "nodes" : %s, "central" : "%s", "central_name" : "%s"}' % (edges_tojson(edges,central.id), nodes_tojson(nodes), central.id, central.name)
    return HttpResponse(answer, content_type='application/json')


@csrf_exempt
def special_nodes(request):
    begin_id = request.POST.get("begin","1")
    end_id = request.POST.get("end", "2")
    level = int(request.POST.get("level", 2))
    if level > 4 or level < 2:
        level = 3
    central = Edge.objects.filter(begin=begin_id).get(end=end_id)
    edges, nodes = neighborhood(central, level)
    answer = '{"edges" : %s, "nodes" : %s, "central" : "%s", "central_name" : "%s"}' % (edges_tojson(edges,central.id), nodes_tojson(nodes), central.id, central.name)
    return HttpResponse(answer, content_type='application/json')


def load_all(request):
    edges = Edge.objects.all()
    lines = []
    for e in edges:
        lines.append("\t".join([str(i) for i in [e.id,e.begin,e.end,e.x1,e.y1,e.x2,e.y2,e.name]]))
    data = "\r\n".join(lines)
    response = HttpResponse(data, content_type='application/csv')
    response['Content-Disposition'] = 'attachment; filename="data.csv"'
    return response


def add(request):
    e = get_object_or_404(Edge, pk=request.POST["id"])    
    name = request.POST["name"]
    print(request.POST["id"])
    if not e.name and name:
        e.name = name
        e.save()
        print(name)
    return HttpResponseRedirect("/graph/")


def edges_tojson(edges, central_id):
    return json.dumps([{"data" : {
        "id" : str(e.id+10000),
        "true_id" : str(e.id),
        "external_id" : str(e.external_id),
        "source" : str(e.begin),
        "target" : str(e.end),
        "name" : e.name + " " + str(e.id) if e.is_initial else e.name + "* " + str(e.id),
        "iscentral" : 1 if e.id == central_id else 0
        }} for e in edges], use_decimal=True)


def nodes_tojson(nodes):
    return json.dumps([{
        "data" : { "id" : str(n.id) },
        "position" : { "x" : n.x, "y" : n.y }            
        } for n in nodes], use_decimal=True)


def neighborhood(central, maxlevel):
    alledges = list(Edge.objects.all())
    neighboors = search(central, alledges, [])
    for i in range(maxlevel-1):
        new_neighboors = neighboors[:]
        for n in neighboors:
            new_neighboors += search(n, alledges, new_neighboors)
        neighboors = new_neighboors[:]

    edges = neighboors
    nodes = []
    nodes_ids = []
    for e in edges:
        if e.begin not in nodes_ids:
            nodes.append(Node(e.begin, e.x1, e.y1))
            nodes_ids.append(e.begin)
        if e.end not in nodes_ids:
            nodes.append(Node(e.end, e.x2, e.y2))
            nodes_ids.append(e.end)

    return edges,nodes


def init(request):
    if not Edge.objects.count():
        with open("v.data") as data:
            lines = [line.split() for line in data.readlines()]
            for line in lines:
                if len(line) > 6:
                    line[6] = ' '.join(line[6:])
                else:
                    line.append("")
        for line in lines:
            edge = Edge(begin=int(line[0]),
                        end=int(line[1]),
                        x1=Decimal(line[2]),
                        y1=Decimal(line[3]),
                        x2=Decimal(line[4]),
                        y2=Decimal(line[5]),
                        name=line[6],
                        is_initial = True if line[6] else False)
            edge.save()

    return HttpResponse(Edge.objects.count())


def search(edge, edges, finded):
    res = []
    for e in edges:
        if e != edge and e not in finded:
            if edge.begin == e.end or edge.begin == e.begin or edge.end == e.begin or edge.end == e.end:
                res.append(e)            
    return res


class StreetForm(forms.Form):
    name = forms.CharField(label='Имя улицы', max_length=200)
    hidden_id = forms.IntegerField(widget=forms.HiddenInput())