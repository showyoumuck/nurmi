from django.db import models
import json

class Edge(models.Model):   
    begin = models.IntegerField(default=0)
    end = models.IntegerField(default=0)
    x1 = models.DecimalField(max_digits=8, decimal_places=2)
    y1 = models.DecimalField(max_digits=8, decimal_places=2)
    x2 = models.DecimalField(max_digits=8, decimal_places=2)
    y2 = models.DecimalField(max_digits=8, decimal_places=2)
    name = models.CharField(default = '', max_length = 200)
    is_initial = models.BooleanField(default=False)
    external_id = models.IntegerField(default=0)

    def __str__(self):
        return "%s -> %s\t%s" % (self.begin, self.end, self.name)
