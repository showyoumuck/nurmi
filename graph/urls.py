from django.conf.urls import patterns, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from graph import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^special/$', views.special, name='special'),
    url(r'^special_nodes/$', views.special_nodes, name='special_nodes'),
    url(r'^add/$', views.add, name='add/'),
    url(r'^init/$', views.init, name='init'),
    url(r'^all/$', views.load_all, name='all')
)
urlpatterns += staticfiles_urlpatterns()