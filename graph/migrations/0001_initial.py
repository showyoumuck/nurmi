# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Edge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('begin', models.IntegerField(default=0)),
                ('end', models.IntegerField(default=0)),
                ('x1', models.DecimalField(max_digits=6, decimal_places=2)),
                ('y1', models.DecimalField(max_digits=6, decimal_places=2)),
                ('x2', models.DecimalField(max_digits=6, decimal_places=2)),
                ('y2', models.DecimalField(max_digits=6, decimal_places=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
