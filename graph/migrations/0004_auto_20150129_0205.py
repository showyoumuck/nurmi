# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graph', '0003_auto_20150121_0624'),
    ]

    operations = [
        migrations.AddField(
            model_name='edge',
            name='external_id',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='edge',
            name='is_initial',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
