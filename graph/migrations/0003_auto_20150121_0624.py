# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('graph', '0002_edge_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='edge',
            name='x1',
            field=models.DecimalField(max_digits=8, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='edge',
            name='x2',
            field=models.DecimalField(max_digits=8, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='edge',
            name='y1',
            field=models.DecimalField(max_digits=8, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='edge',
            name='y2',
            field=models.DecimalField(max_digits=8, decimal_places=2),
            preserve_default=True,
        ),
    ]
