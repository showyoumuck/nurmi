from django.conf.urls import patterns, include, url
from django.contrib import admin
from graph import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'graph_site.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^graph/', include('graph.urls')),
)
urlpatterns += staticfiles_urlpatterns()
